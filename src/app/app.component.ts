import { Component, OnInit } from '@angular/core';
import { DataService } from "./data.service";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
// export class AppComponent implements OnInit {
//   constructor(private serve:DataService){

//   }
//   title = 'first-app';
//   ngOnInit(){
// this.serve.send();
//   }
  
// }
export class AppComponent {
title = 'Angular 7 Project!';
  public persondata : any;
  constructor(private myservice: DataService) {}
  ngOnInit() {
     this.myservice.getData().subscribe((data) => {
        this.persondata =data;
          console.log(this.persondata);
     });
  }
}
