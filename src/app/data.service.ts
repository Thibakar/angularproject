import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";
import { map } from "rxjs/operators";



@Injectable({
  providedIn: 'root'
})
export class DataService {
//  const apiurl = "https://reqres.in/api/users?page=1";
//   constructor(private http:HttpClient) { }
//   send(){
//    var dat=this.http.get(this.API_URL)
//     return this.http.get(this.apiurl);
//   }
private finaldata = [];
private apiurl = "http://jsonplaceholder.typicode.com/posts";
constructor(private http: HttpClient) { }
getData() {
   return this.http.get(this.apiurl);
}
}
